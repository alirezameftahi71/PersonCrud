# creation of the database 
DROP DATABASE IF EXISTS person_crud;
CREATE DATABASE person_crud;
USE person_crud;

# creation of the tables;
CREATE TABLE address (
	id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	city varchar(30),
	street_main(30),
	street_second(30),
	block int,
	zip_code int
);
CRAETE TABLE person (
	id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	f_name varchar(30),
	l_name varchar(30),
	tel varchar(20),
	address_id int REFERENCES address(id)
);