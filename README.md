# PersonCrud
CRUD RESTfull services working with Jersey and JDBC

First Clone the project in eclipse.
Then in mysql make a database using these commands: 

<pre>
DROP DATABASE IF EXISTS person_crud;
CREATE DATABASE person_crud;
USE person_crud;
CREATE TABLE address(
	id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	city varchar(30),
	street_main varchar(30),
	street_second varchar(30),
	block int,
	zip_code int
);
CREATE TABLE person(
	id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	f_name varchar(30),
	l_name varchar(30),
	tel varchar(20),
	address_id int REFERENCES address(id)
);
</pre>

Then go to the Java file `DbConnector.java` located in `/src/database/DbConnector.java` and configure the file with your own mysql username and password.
Create a tomcat server for this project in eclipse and then run it. 
Now you can open a browser and go to address : `http://localhost:8080/PersonCrud/index.html` and in the home page start using the webservices.
