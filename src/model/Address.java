package model;

public class Address {

	// fields
	private int id;
	private String city;
	private String streetMain;
	private String streetSecond;
	private int block;
	private int zipCode;

	// getters and setters
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the streetMain
	 */
	public String getStreetMain() {
		return streetMain;
	}

	/**
	 * @param streetMain
	 *            the streetMain to set
	 */
	public void setStreetMain(String streetMain) {
		this.streetMain = streetMain;
	}

	/**
	 * @return the streetSecond
	 */
	public String getStreetSecond() {
		return streetSecond;
	}

	/**
	 * @param streetSecond
	 *            the streetSecond to set
	 */
	public void setStreetSecond(String streetSecond) {
		this.streetSecond = streetSecond;
	}

	/**
	 * @return the block
	 */
	public int getBlock() {
		return block;
	}

	/**
	 * @param block
	 *            the block to set
	 */
	public void setBlock(int block) {
		this.block = block;
	}

	/**
	 * @return the zipCode
	 */
	public int getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

}
