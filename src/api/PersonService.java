package api;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import database.DbConnector;
import model.Address;
import model.Person;

// full root path : http://localhost:8080/PersonCrud/api/person/services/

@Path("/person/services")
public class PersonService {

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public void add(Person person) {
		String queryAddress = "INSERT INTO address(id, city, street_main, street_second, block, zip_code) VALUES("
				+ person.getAddress().getId() + ", '" + person.getAddress().getCity() + "', '"
				+ person.getAddress().getStreetMain() + "', '" + person.getAddress().getStreetSecond() + "', "
				+ person.getAddress().getBlock() + ", " + person.getAddress().getZipCode() + ");";
		String queryPerson = "INSERT INTO person(id, f_name, l_name, tel, address_id) VALUES(" + person.getId() + ", '"
				+ person.getfName() + "', '" + person.getlName() + "', '" + person.getTel() + "', "
				+ person.getAddress().getId() + ");";
		DbConnector.startConnection();
		try {
			DbConnector.getStatement().executeUpdate(queryAddress);
			DbConnector.getStatement().executeUpdate(queryPerson);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbConnector.closeConnection();
		}
	}

	@GET
	@Path("/showall")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Person> getAll() {
		ArrayList<Person> persons = new ArrayList<>();
		String query = "SELECT person.*, address.* FROM person "
				+ "INNER JOIN address ON person.address_id = address.id ORDER BY person.id DESC;";
		DbConnector.startConnection();
		try {
			ResultSet rs = DbConnector.getStatement().executeQuery(query);
			while (rs.next()) {
				Address address = new Address();
				Person person = new Person();
				address.setId(rs.getInt("address_id"));
				address.setCity(rs.getString("city"));
				address.setStreetMain(rs.getString("street_main"));
				address.setStreetSecond(rs.getString("street_second"));
				address.setBlock(rs.getInt("block"));
				address.setZipCode(rs.getInt("zip_code"));

				person.setId(rs.getInt("id"));
				person.setfName(rs.getString("f_name"));
				person.setlName(rs.getString("l_name"));
				person.setTel(rs.getString("tel"));
				person.setAddress(address);

				persons.add(person);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			DbConnector.closeConnection();
		}
		return persons;
	}

	@GET
	@Path("/getperson/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Person getById(@PathParam("id") int id) {
		Person person = new Person();
		Address address = new Address();
		String query = "SELECT person.*, address.* FROM person "
				+ "INNER JOIN address ON person.address_id = address.id " + "WHERE person.id = " + id + ";";
		DbConnector.startConnection();
		try {
			ResultSet rs = DbConnector.getStatement().executeQuery(query);
			rs.next();
			address.setId(rs.getInt("address_id"));
			address.setCity(rs.getString("city"));
			address.setStreetMain(rs.getString("street_main"));
			address.setStreetSecond(rs.getString("street_second"));
			address.setBlock(rs.getInt("block"));
			address.setZipCode(rs.getInt("zip_code"));

			person.setId(rs.getInt("id"));
			person.setfName(rs.getString("f_name"));
			person.setlName(rs.getString("l_name"));
			person.setTel(rs.getString("tel"));
			person.setAddress(address);
		} catch (SQLException e) {
			// e.printStackTrace();
			System.out.println("Error getting data out of the database.");
			return null;
		} finally {
			DbConnector.closeConnection();
		}
		return person;
	}

	@DELETE
	@Path("/delete/{id}")
	public Response delete(@PathParam("id") int id) {
		String queryAddress = "DELETE FROM address WHERE id = " + id + ";";
		String queryPerson = "DELETE FROM person WHERE id = " + id + ";";
		DbConnector.startConnection();
		try {
			DbConnector.getStatement().executeUpdate(queryAddress);
			DbConnector.getStatement().executeUpdate(queryPerson);
			return Response.ok().build();
		} catch (SQLException se) {
			se.printStackTrace();
			return Response.serverError().build();
		} finally {
			DbConnector.closeConnection();
		}
	}

	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public void update(Person person) {
		Person p = getById(person.getId());
		if (p != null) {
			String queryAddress = "UPDATE address SET city = '" + person.getAddress().getCity() + "', street_main = '"
					+ person.getAddress().getStreetMain() + "', street_second = '"
					+ person.getAddress().getStreetSecond() + "', block = " + person.getAddress().getBlock()
					+ ", zip_code = " + person.getAddress().getZipCode() + " WHERE id = " + person.getAddress().getId()
					+ ";";
			String queryPerson = "UPDATE person SET f_name = '" + person.getfName() + "', l_name = '"
					+ person.getlName() + "', tel = '" + person.getTel() + "', address_id = "
					+ person.getAddress().getId() + " WHERE id = " + person.getId() + ";";
			DbConnector.startConnection();
			try {
				DbConnector.getStatement().executeUpdate(queryAddress);
				DbConnector.getStatement().executeUpdate(queryPerson);
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				DbConnector.closeConnection();
			}
		} else {
			System.out.println("NULL returned.");
		}
	}

}
